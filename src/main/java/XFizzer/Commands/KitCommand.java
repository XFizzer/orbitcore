package XFizzer.Commands;

import cn.nukkit.Player;
import cn.nukkit.command.CommandSender;
import cn.nukkit.command.Command;
import cn.nukkit.item.Item;
import XFizzer.Main;

public class KitCommand extends Command {

		private Main plugin;

		public KitCommand(Main plugin) {
				super("kit", "Kits for your server.", "/kit <start|vip>");
				this.plugin = plugin;
		}

		public boolean execute(CommandSender sender, String commandLabel, String[] args) {
				if (sender instanceof Player) {
						if (args.length != 0) {
								switch (args[0]) {
										case "start":
												Player player = (Player) sender;
												Item item1 = Item.get(272, 0, 1).setCustomName("§2Starter Sword");
												Item item2 = Item.get(273, 0, 1).setCustomName("§2Starter Shovel");
												Item item3 = Item.get(274, 0, 1).setCustomName("§7[§aStarer§7]§a Pickaxe");
												player.getInventory().addItem(item1, item2, item3);
												break;
										case "hand":
												break;
								}
						}
				}
				return true;
		}
}