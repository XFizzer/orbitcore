package XFizzer.Commands;

import cn.nukkit.Player;
import cn.nukkit.command.CommandSender;
import cn.nukkit.command.Command;
import cn.nukkit.level.Location;
import cn.nukkit.level.Level;
import XFizzer.Main;

public class SpawnCommamd extends Command {

    private Main plugin;

    public SpawnCommamd(Main plugin) {
        super("spawn", "Go to spawn.", "/spawn");
        this.plugin = plugin;
    }
    
    public boolean execute(CommandSender sender, String s, String[] args) {
        Player player = (Player) sender;
        player.teleport(Location.fromObject(this.plugin.getServer().getDefaultLevel().getSpawnLocation(), this.plugin.getServer().getDefaultLevel()));
        return true;
    }
}