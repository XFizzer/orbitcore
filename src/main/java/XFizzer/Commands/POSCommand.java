package XFizzer.Commands;

import cn.nukkit.Player;
import cn.nukkit.command.CommandSender;
import cn.nukkit.command.Command;
import cn.nukkit.item.Item;
import XFizzer.Main;

public class POSCommand extends Command {

    private Main plugin;

    public POSCommand(Main plugin) {
        super("pos", "Get position of a player.", "/pos");
        this.plugin = plugin;
    }
    
    public boolean execute(CommandSender sender, String s, String[] args) {
        Player player = (Player) sender;
        sender.sendMessage( "§2X:§3 " + player.getFloorX() + ", " + "§2Y:§3 " + player.getFloorY() + ", " + "§2Z:§3 " + player.getFloorZ());
        return true;
    }
}