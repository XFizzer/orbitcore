package XFizzer.Commands;

import cn.nukkit.Player;
import cn.nukkit.command.CommandSender;
import cn.nukkit.command.Command;
import cn.nukkit.item.Item;
import XFizzer.Main;

public class RepairCommand extends Command {

    private Main plugin;

    public RepairCommand(Main plugin) {
        super("repair", "Repair any item in your inventory.", "/mwcreate");
        this.plugin = plugin;
    }
    
    public boolean execute(CommandSender sender, String s, String[] args) {
        Player player = (Player) sender;
        sender.sendMessage("Item Repaired");
        Item item = player.getInventory().getItemInHand();
        item.setDamage(0);
        player.getInventory().setItemInHand(item);
        return true;
    }
}