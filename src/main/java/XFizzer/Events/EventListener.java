package XFizzer.Events;

import cn.nukkit.event.EventHandler;
import cn.nukkit.event.EventPriority;
import cn.nukkit.event.Listener;
import cn.nukkit.Server;
import cn.nukkit.Player;
import cn.nukkit.event.player.PlayerInteractEvent;
import cn.nukkit.item.Item;
import cn.nukkit.event.player.PlayerJoinEvent;
import cn.nukkit.event.player.PlayerQuitEvent;
import cn.nukkit.event.block.BlockBreakEvent;
import XFizzer.Main;

public class EventListener implements Listener {

		Main plugin;

		public EventListener(Main plugin) {
				this.plugin = plugin;
		}

		@EventHandler
		public void onJoin(PlayerJoinEvent event) {
				event.setJoinMessage("");
				int messages = 0;
				int rewards = 0;
				Player player = event.getPlayer();
				player.sendMessage("------------------------");
				player.sendMessage("");
				player.sendMessage("Welcome " + player.getName() + " To OmegaOPPrison");
				player.sendMessage("§eYou have " + messages + " new messages!");
				player.sendMessage("You have " + rewards + " rewards to claim!");
				player.sendMessage("");
				player.sendMessage("------------------------");
		}

		@EventHandler
		public void onQuit(PlayerQuitEvent event) {
				event.setQuitMessage("");
		}

		@EventHandler
		public void onBlockBreak(BlockBreakEvent event) {
				Player player = event.getPlayer();
				if (event.isCancelled() == false) {
						if (player.getInventory().canAddItem(event.getItem())) {
								player.getInventory().addItem(event.getDrops());
						}else{
								player.sendMessage("§aInventory Full");
								event.setCancelled();
						}
						event.setDrops(null);
				}
		}
		
		@EventHandler
		public void onTap(PlayerInteractEvent e){
				Player player = e.getPlayer();
				Item item = player.getInventory().getItemInHand();
		}
}