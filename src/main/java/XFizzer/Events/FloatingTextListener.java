package XFizzer.Events;

import cn.nukkit.event.EventHandler;
import cn.nukkit.event.EventPriority;
import cn.nukkit.event.Listener;
import cn.nukkit.event.player.PlayerJoinEvent;
import cn.nukkit.event.player.PlayerQuitEvent;
import cn.nukkit.level.particle.FloatingTextParticle;
import cn.nukkit.math.Vector3;
import cn.nukkit.level.particle.Particle;
import cn.nukkit.level.Level;
import cn.nukkit.Player;
import XFizzer.Main;

public class FloatingTextListener implements Listener {

		Main plugin;

		public FloatingTextListener(Main plugin) {
				this.plugin = plugin;
		}

		@EventHandler
		public void onJoin(PlayerJoinEvent event) {
				Player player = event.getPlayer();
				Level level = this.plugin.getServer().getLevelByName("World");
				FloatingTextParticle particle = new FloatingTextParticle(new Vector3(115.5, 8, 121.5), "", "-= Welcome to OrbitPE =-");
				FloatingTextParticle particle1 = new FloatingTextParticle(new Vector3(115.5, 7.7, 121.5), "", "" + player.getName());
				level.addParticle(particle, player);
				level.addParticle(particle1, player);
		}
}