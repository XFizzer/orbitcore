package XFizzer;

import java.io.File;
import java.util.*;

import cn.nukkit.Player;
import cn.nukkit.utils.TextFormat;
import cn.nukkit.plugin.PluginBase;
import cn.nukkit.Server;
import cn.nukkit.event.Listener;
import XFizzer.Events.EventListener;
import XFizzer.Events.FloatingTextListener;
import XFizzer.Commands.RepairCommand;
import XFizzer.Commands.POSCommand;
import XFizzer.Commands.SpawnCommamd;
import XFizzer.Commands.KitCommand;

public class Main extends PluginBase implements Listener {

		@Override
		public void onEnable() {
			this.getLogger().info(TextFormat.DARK_GREEN + "OrbitCore enabled!");
			getServer().getPluginManager().registerEvents(this, this);
			/*this.getServer().loadLevel("world");*/
			this.registerCommands();
			this.registerEvents();
		}

		private void registerCommands() {
				this.getServer().getCommandMap().register("repair", new RepairCommand(this));
				this.getServer().getCommandMap().register("pos", new POSCommand(this));
				this.getServer().getCommandMap().register("spawn", new SpawnCommamd(this));
				this.getServer().getCommandMap().register("kit", new KitCommand(this));
		}

		private void registerEvents() {
				this.getServer().getPluginManager().registerEvents(new FloatingTextListener(this), this);
				this.getServer().getPluginManager().registerEvents(new EventListener(this), this);
		}
}